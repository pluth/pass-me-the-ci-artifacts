#include <iostream>

class Foo {
    int c;

public:
    Foo() : c(1) {}
    int doStuff(void) {
        return 0;
    }
};

int ab() {
    int b = 1;
    return b;
}

int main() {
    int a = 0;

    ab();
    auto foo = Foo();
    foo.doStuff();

    std::cout << "hello " << a << "\n";
}